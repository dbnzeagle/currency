export const ExchangeVal = [
    {
        value: 'AUD',
        label: 'Австралийский доллар',
        item: 'A$',
    },
    {
        value: 'AZN',
        label: 'Азербайджанский манат',
        item: '₼',

    },
    {
        value: 'GBP',
        label: 'Фунт стерлингов',
        item: '£',

    },
    {
        value: 'AMD',
        label: 'Армянский драм',
        item: 'Դ',

    },
    {
        value: 'BYN',
        label: 'Белорусский рубль',
        item: 'Br',

    },
    {
        value: 'BGN',
        label: 'Болгарский лев',
        item: 'лв',

    },
    {
        value: 'BRL',
        label: 'Бразильский реал',
        item: 'R$',

    },
    {
        value: 'HUF',
        label: 'Венгерский форинт',
        item: 'ƒ',

    },
    {
        value: 'HKD',
        label: 'Гонконгский доллар',
        item: 'HK$',

    },
    {
        value: 'DKK',
        label: 'Датская крона',
        item: 'kr',

    },
    {
        value: 'USD',
        label: 'Доллар США',
        item: '$',

    },
    {
        value: 'EUR',
        label: 'Евро',
        item: '€',

    },
    {
        value: 'INR',
        label: 'Индийский рупий',
        item: '₹',

    },
    {
        value: 'KZT',
        label: 'Казахстанский тенге',
        item: '₸',

    },
    {
        value: 'CAD',
        label: 'Канадский доллар',
        item: "C$",

    },
    {
        value: 'KGS',
        label: 'Киргизский сом',
        item: 'с',

    },
    {
        value: 'CNY',
        label: 'Китайский юань',
        item: '¥',

    },
    {
        value: 'MDL',
        label: 'Молдавский лей',
        item: 'L',

    },
    {
        value: 'NOK',
        label: 'Норвежская крона',
        item: 'kr',

    },
    {
        value: 'PLN',
        label: 'Польский злотый',
        item: 'zł',

    },
    {
        value: 'RON',
        label: 'Румынский лей',
        item: 'L',

    },
    {
        value: 'XDR',
        label: 'СДР (специальные права заимствования)',
        item: 'SDR',

    },
    {
        value: 'SGD',
        label: 'Сингапурский доллар',
        item: 'S$',

    },
    {
        value: 'TJS',
        label: 'Таджикский сомони',
        item: 'с.',

    },
    {
        value: 'TRY',
        label: 'Турецкая лира',
        item: '₺',

    },
    {
        value: 'TMT',
        label: 'Новый туркменский манат',
        item: 'm',

    },
    {
        value: 'UZS',
        label: 'Узбекский сум',
        item: 'so’m',

    },
    {
        value: 'UAH',
        label: 'Украинская гривна',
        item: '₴',

    },
    {
        value: 'CZK',
        label: 'Чешская крона',
        item: 'Kč',

    },
    {
        value: 'SEK',
        label: 'Шведская крона',
        item: 'kr',

    },
    {
        value: 'CHF',
        label: 'Швейцарский франк',
        item: '₣ ',

    },
    {
        value: 'ZAR',
        label: 'Южноафриканский рэнд',
        item: 'R',

    },
    {
        value: 'KRW',
        label: 'Южнокорейская вона',
        item: '₩',

    },
    {
        value: 'JPY',
        label: 'Японский иен',
        item: '¥',

    },

];
export const Stock = [
    'MSFT',
    'AMZN',
    'GOOG',
    'AAPL',
    'PG',
    'FB',
    'BABA',
    'JNJ',
];
