const tintColor = '#f9aa33';

export default {
    tintColor,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColor,
    tabBar: '#4e4f54',
    errorBackground: 'red',
    errorText: '#fff',
    warningBackground: '#EAEB5E',
    warningText: '#666804',
    noticeBackground: tintColor,
    noticeText: '#fff',
    platinum:'#e5e4e2',
    paladium:'#6F6A75',
    silver:'#c0c0c0',
    up:'#5F8961',
    down:'#B60006',
};
