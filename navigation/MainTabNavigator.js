import React from 'react';
import {createStackNavigator, createBottomTabNavigator, createAppContainer} from 'react-navigation';
import HomeScreen from '../screens/HomeScreen'
import BirgeScreen from "../screens/BirgeScreen";
import InfoScreen from "../screens/InfoScreen"
import Theme from "../constants/Theme";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


const HomeStack = createStackNavigator({
    Home: {screen: HomeScreen},
});

const BirgeStack = createStackNavigator({
    Birge: {screen: BirgeScreen},
    Info: {screen: InfoScreen},

});

export default createAppContainer(
    createBottomTabNavigator(
        {
            Курс: {screen: HomeStack},
            Биржа: {screen: BirgeStack},
        },
        {
            defaultNavigationOptions: ({navigation}) => ({
                tabBarIcon: ({focused,tintColor}) => {
                    const {routeName} = navigation.state;
                    let iconName;
                    if (routeName === 'Курс') {
                        iconName = `cash-multiple`;
                    } else if (routeName === 'Биржа') {
                        iconName = `chart-line`;
                    }


                    return <Icon name={iconName} size={32} color={tintColor}/>;
                },
            }),
            tabBarOptions: {
                style: {
                    backgroundColor: Theme.PRIMARY_LIGHT_COLOR
                },
                activeTintColor: Theme.SECONDARY_LIGHT_COLOR,
                inactiveTintColor: 'gray',

            },
        }
    ));
