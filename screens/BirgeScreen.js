import React from 'react';
import {View} from 'react-native';
import Theme from "../constants/Theme";
import StockValues from "../components/StockValues";
import {Stock} from "../constants/Data";


export default class BirgeScreen extends React.Component {
    static navigationOptions = {
        header: null,
        title: 'Birge'
    };

    render() {
        return (
            <View
                style={{backgroundColor: Theme.PRIMARY_COLOR, flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <StockValues list={Stock}
                                 nav={this.props.navigation}/>

            </View>
        );
    }
}
