import React from 'react';
import {Dimensions,StyleSheet,ScrollView, Text, View} from 'react-native';
import Theme from "../constants/Theme";
import ExchangeValues from "../components/ExchangeValues";
import {Dropdown} from "react-native-material-dropdown";
import XeValues from "../components/XeValues";
import {ExchangeVal} from "../constants/Data";

export default class HomeScreen extends React.Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);
        this.state = {
            name: "",
            symbol: "",
        };
    }


    render() {
        const {symbol} = this.state;
        return (
            <View style={{
                backgroundColor: Theme.PRIMARY_COLOR,
                paddingTop:56,
                flex: 1,
                justifyContent: 'flex-start',
                alignItems: 'center'
            }}>
                <View/>
                <ScrollView>
                <View style={styles.cardView}>
                    <View style={styles.dropdownView}>
                        <Dropdown
                            animationDuration={300}
                            containerStyle={{width: "70%"}}
                            baseColor={Theme.PRIMARY_TEXT_COLOR}
                            itemCount={8}
                            dropdownPosition={0}
                            textColor={Theme.SECONDARY_COLOR}
                            label='Валюта'
                            data={ExchangeVal}
                            valueExtractor={({value}) => value}
                            onChangeText={(key) => {
                                this.setState({
                                    name: key,
                                    symbol: ExchangeVal.find(element => element.value === key).item
                                });
                            }}
                        />
                        <Text style={styles.valueView}>{symbol}</Text>
                    </View>

                    <ExchangeValues name={this.state.name}/>

                </View>
                <View style={styles.metalView}>
                        <XeValues/>
                </View>
                </ScrollView>

            </View>

        );

    }
}

const styles = StyleSheet.create({
    metalView:{
        flex:1,
        justifyContent: 'center',
        borderRadius: 16,
        backgroundColor: Theme.PRIMARY_LIGHT_COLOR,
        marginTop: 10,
        width: Dimensions.get('window').width * 0.95 ,
    },
    valueView: {
        color: Theme.PRIMARY_TEXT_COLOR,
        fontSize: 25,
        marginTop: 16,
        marginBottom: 10,
        marginLeft: 25,
    },
    cardView: {
        justifyContent: 'center',
        borderRadius: 16,
        backgroundColor: Theme.PRIMARY_LIGHT_COLOR,
        width: Dimensions.get('window').width * 0.95,
        zIndex: 100,

    },
    dropdownView: {
        zIndex: 1,
        flexDirection: 'row',
        width: Dimensions.get('window').width * 0.9,
        paddingLeft: 16,
    },
});
