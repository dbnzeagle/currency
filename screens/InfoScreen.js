import React from 'react';
import {Platform, ScrollView, Dimensions, StyleSheet, Text,Button, View} from 'react-native';
import Theme from "../constants/Theme";
import {LineChart} from "react-native-chart-kit";
import {Stock} from "../constants/Data";
import DotValue from "../components/DotValue";
import {PulseIndicator} from "react-native-indicators";
//import {Header} from "react-native-elements";
import {ApiKey} from "../constants/ApiKey";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";


export default class InfoScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        return {
            tabBarVisible: false,
            headerStyle: {
                backgroundColor: Theme.SECONDARY_COLOR,
            },
            headerLeft: (
                <MaterialCommunityIcons name={"arrow-left"} style={{ paddingLeft:15,color: Theme.SECONDARY_TEXT_COLOR }} size={25} onPress={() => navigation.goBack()} />
            ),
        }

    };

    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            value: [],
            days: [],
            singleVal: 0,
            company: "",
            companyName: "",
            dayValue: "Выберите точку на графике для получения информации"
        };
    }

    componentDidMount() {
        this.fetchValues();
    }

    componentWillUnmount() {

    }

    async fetchValues() {
        let index = this.props.navigation.getParam('itemId', 0);
        let companyFull = this.props.navigation.getParam('company', 0);
        let value = Stock[index];
        let tmp = [];
        let dates = [];
        await fetch(
            `https://cloud.iexapis.com/beta/stock/${value}/chart/5d?token=${ApiKey}`
        )
            .then(res => res.json())
            .then(json => {
                tmp = json.map(el => el.close);
                dates = json.map(el => el.date.substring(8, 10));
            });

        this.setState({
            companyName: companyFull,
            company: value,
            value: tmp,
            days: dates,
            isLoading: false,
        })
    };

    render() {
        const {isLoading, singleVal, companyName, company, value, days} = this.state;

        return (
            <View
                style={{
                    backgroundColor: Theme.PRIMARY_COLOR,
                    flex: 1,
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                }}>
                {isLoading ? (

                    <PulseIndicator color={Theme.SECONDARY_COLOR}
                                    size={128}/>) : (

                    <ScrollView>
                        <View style={{
                            marginTop: 16,
                            color: Theme.PRIMARY_LIGHT_COLOR
                        }}>
                            <DotValue item={company} itemName={companyName}/>
                            <LineChart
                                onDataPointClick={({value}) => {
                                    this.setState({
                                        singleVal: value,
                                    })
                                }}
                                data={{
                                    labels: days,
                                    datasets: [{
                                        data: value
                                    }]
                                }}
                                width={Dimensions.get('window').width * 0.95} // from react-native
                                height={220}
                                withDots={true}
                                yAxisLabel={'$'}
                                chartConfig={{
                                    width: 60,
                                    strokeWidth: 2,
                                    backgroundColor: Theme.PRIMARY_DARK_COLOR,
                                    backgroundGradientFrom: Theme.PRIMARY_LIGHT_COLOR,
                                    backgroundGradientTo: Theme.SECONDARY_DARK_COLOR,
                                    decimalPlaces: 2, // optional, defaults to 2dp
                                    color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,

                                }}
                                style={{
                                    zIndex: 300,
                                    marginLeft: 10,
                                    marginRight: 10,
                                    marginBottom: 16,
                                    borderRadius: 16
                                }}
                            />
                            {Platform.OS === 'ios' ? (null) : (
                                <View style={styles.dataView}>
                                    <Text style={styles.valuesText}>
                                        Цена акции : {singleVal} USD{"\n"}
                                    </Text>
                                </View>
                            )}

                        </View>
                    </ScrollView>
                )}


            </View>
        );
    }
}
const styles = StyleSheet.create({
    dataView: {
        borderRadius: 16,
        marginBottom: 16,
        marginLeft: 10,
        marginRight: 10,
        backgroundColor: Theme.PRIMARY_LIGHT_COLOR,
    },
    valuesText: {
        color: Theme.SECONDARY_COLOR,
        fontSize: 16,
        paddingTop: 16,
        paddingLeft: 16,
        paddingBottom: 16,
    }
});