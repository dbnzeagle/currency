import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Theme from "../constants/Theme";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from "../constants/Colors";

export default class ExchangeValues extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cost: 0,
            count: 0,
            oldCost: 0,
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.name !== this.props.name) {
            this.fetchValues();
        }
    }


    fetchValues() {
        fetch(
            `https://www.cbr-xml-daily.ru/daily_json.js`
        )
            .then(res => res.json())
            .then(json => {

                this.setState({
                    oldCost: json.Valute[this.props.name].Previous,
                    cost: json.Valute[this.props.name].Value,
                    count: json.Valute[this.props.name].Nominal,
                });

            });
    };

    render() {
        const {cost, count, oldCost} = this.state;
        let dir = oldCost - cost;
        return (

            <View>
                <View style={styles.textView}>
                    <Text style={styles.nominal}>{count + " "}{this.props.name}</Text>
                    {dir < 0 ?
                        <Text style={styles.text}>{cost} ₽
                            <Icon name={`chevron-up`} size={32} color={Colors.up}/>
                        </Text> :
                        <Text style={styles.text}>{cost} ₽
                            <Icon name={`chevron-down`} size={32} color={Colors.down}/>
                        </Text>}
                </View>
            </View>
        );
    };
}

const styles = StyleSheet.create({
    nominal: {
        color: Theme.SECONDARY_COLOR,
        fontSize: 25
    },
    text: {
        marginLeft: 'auto',
        color: Theme.PRIMARY_TEXT_COLOR,
        fontSize: 48,
    },
    textView: {
        paddingTop: 16,
        paddingBottom:16,
        marginRight: 10,
        marginBottom: 'auto',
        marginLeft: 'auto',
    }
});
