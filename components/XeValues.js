import React from 'react';
import {Text,View, FlatList, StyleSheet, Dimensions} from 'react-native';

import Theme from "../constants/Theme";
import moment from "moment";

export default class XeValues extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cost: [],
            updateDate:0,
        };

    }

    componentDidMount() {
        this.fetchValues();

    }


    async fetchValues() {
        const parseString = require('react-native-xml2js').parseString;
        let check=moment().format('ddd');
        let today;
        if(check==='Sun'){
            today = moment().subtract(1, 'days').format('DD/MM/YYYY');
        }
        else if(check==='Mon'){
            today = moment().subtract(2, 'days').format('DD/MM/YYYY');
        }
        else
        {
            today = moment().format('DD/MM/YYYY');
        }
        let data = [];

        await fetch(
            `http://www.cbr.ru/scripts/xml_metall.asp?date_req1=${today}&date_req2=${today}`
        )
            .then(res => res.text())
            .then((text) => {
                parseString(text, function (err, result) {
                    data = result.Metall.Record;
                });
            });
        this.setState({
            cost: data,
            updateDate:today
        });

    }

    renderItem({item, index}) {
        return <View style={{
            borderRadius:10,
            justifyContent:'center',
            flexDirection: 'row',
            margin:5,
            marginLeft: 10,
            marginRight: 10,
            height: Dimensions.get('window').width*0.13,
            width:Dimensions.get('window').width*0.8,
            backgroundColor: Theme.SECONDARY_COLOR,
        }}>
            <Text style={styles.text}>{metal.find(element=>element.index===index).value} {item.Buy}</Text>
        </View>
    }


    render() {
        const {cost,updateDate} = this.state;
        return (
            <View style={styles.listView}>
                <Text style={{color: Theme.PRIMARY_TEXT_COLOR}}>Дата обновления: {updateDate}</Text>
                <Text style={{color: Theme.PRIMARY_TEXT_COLOR,fontSize: 12}}>рублей за грамм</Text>
                <FlatList
                    style={{paddingTop:16}}
                    numColumns={1}
                    data={cost}
                    renderItem={this.renderItem}
                />

            </View>
        );
    }
}

const styles = StyleSheet.create({
    text: {

        paddingTop:16,
        paddingLeft: 10,
        color: Theme.SECONDARY_TEXT_COLOR,
        fontSize: 16,
    },
    listView:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        paddingTop: 32,
        paddingBottom: 8,
    },

});
const metal=[
    {
        index:0,
        value:'Золото:'
    },
    {
        index:1,
        value:'Серебро:'
    },
    {
        index:2,
        value:'Платина:'
    },
    {
        index:3,
        value:'Палладий:'
    }
];