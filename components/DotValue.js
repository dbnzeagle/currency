import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Theme from "../constants/Theme";
import moment from "moment";
import 'moment/locale/ru'
import {ApiKey} from "../constants/ApiKey";

export default class DotValue extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: [],
            isLoading: true,
        }
    }

    componentDidMount() {
        this.fetchValues()
    }

    async fetchValues() {
        let value = this.props.item;
        let tmp = [];
        await fetch(
            `https://cloud.iexapis.com/beta/stock/${value}/quote?token=${ApiKey}`
        )
            .then(res => res.json())
            .then(json => {
                tmp.push({
                    previousClose: json.previousClose,
                    marketCap: json.marketCap,
                    week52High: json.week52High,
                    week52Low: json.week52Low,
                })
            });

        this.setState({
            value: tmp,
            isLoading: false,
        })

    }

    render() {
        const {value} = this.state;
        let now = moment();

        return (
            <View style={styles.dataView}>
                <Text numberOfLines={2} style={styles.text}>{this.props.item}{"\n"}
                    {this.props.itemName}
                </Text>
                <Text style={styles.valuesText}>

                    Месяц: {now.format('MMMM').charAt(0).toUpperCase()+now.format('MMMM').slice(1)} {"\n"}
                    Капитализация : {(value.map(el => el.marketCap) / 1000000000).toFixed(1)} млрд. USD{"\n"}
                    52 нед. макс. : {value.map(el => el.week52High)} USD{"\n"}
                    52 нед. мин. : {value.map(el => el.week52Low)} USD{"\n"}
                </Text>
            </View>
        );
    };
}

const styles = StyleSheet.create({
    text: {
        color: Theme.SECONDARY_COLOR,
        fontSize: 32,
        paddingTop: 16,
        paddingBottom: 16,
        marginRight: 'auto',
        marginBottom: 'auto',
        marginLeft: 10,
    },
    dataView: {

        borderRadius: 16,
        marginBottom: 16,
        marginLeft: 10,
        marginRight: 10,
        backgroundColor: Theme.PRIMARY_LIGHT_COLOR,
    },
    valuesText: {
        color: Theme.SECONDARY_COLOR,
        fontSize: 16,
        paddingTop: 16,
        paddingBottom: 16,
        marginRight: 'auto',
        marginBottom: 'auto',
        marginLeft: 10,
    }
});
