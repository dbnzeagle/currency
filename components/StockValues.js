import React, {Component} from 'react';
import {StyleSheet, View, Text, Dimensions, TouchableWithoutFeedback} from 'react-native';
import {FlatGrid} from 'react-native-super-grid';
import Theme from "../constants/Theme";
import {PulseIndicator} from "react-native-indicators";
import Colors from "../constants/Colors";
import {ApiKey} from "../constants/ApiKey";


export default class StockValues extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            value: [],
        };
    }

    componentDidMount() {
        this.fetchValues();
    }

    async fetchValues() {
        let data = this.props.list;
        let tmp = [];
        var i;
        for (i = 0; i < data.length; i++) {
            await fetch(
                `https://cloud.iexapis.com/beta/stock/${data[i]}/quote?token=${ApiKey}`
            )
                .then(res => res.json())
                .then(json => {
                    tmp.push({
                        symbol: json.symbol,
                        companyName: json.companyName,
                        latestPrice: json.latestPrice,
                        change: json.change,
                        changePercent: json.changePercent
                    });
                });
        }
        this.setState({
            value: tmp,
            isLoading: false
        })
    }

    render() {
        const {isLoading, value} = this.state;
        let res=0;
        if(Dimensions.get('window').width<=320){
            res=200
        }
        else {res=125}
        return (
            <View>
                {isLoading ? (
                    <PulseIndicator color={Theme.SECONDARY_COLOR}
                                    size={128}/>
                ) : (

                        <FlatGrid
                            itemDimension={res}
                            items={value}
                            style={styles.gridView}
                            renderItem={({item, index}) => (

                                <TouchableWithoutFeedback onPress={() => this.props.nav.navigate('Info', {
                                    itemId: index,
                                    company: item.companyName
                                })}>
                                    <View style={[styles.itemContainer, {backgroundColor: Theme.SECONDARY_COLOR}]}>
                                        <Text style={styles.itemName}>{item.companyName}</Text>
                                        <Text style={styles.itemPrice}>{item.latestPrice} USD</Text>

                                        <View style={{flexDirection: 'row'}}>
                                            {item.change < 0 ? (
                                                <Text style={styles.itemChangeDown}>{item.change} </Text>
                                            ) : (
                                                <Text style={styles.itemChangeUp}>+{item.change} </Text>
                                            )}
                                            {item.change < 0 ? (
                                                <Text
                                                    style={styles.itemChangePercentDown}>({(item.changePercent * 100).toFixed(2)} %)</Text>
                                            ) : (
                                                <Text
                                                    style={styles.itemChangePercentUp}>({(item.changePercent * 100).toFixed(2)} %)</Text>
                                            )}
                                        </View>

                                    </View>
                                </TouchableWithoutFeedback>
                            )}
                        />
                )}
            </View>
        );
    }

}

const styles = StyleSheet.create({
    gridView: {
        marginTop: 56,
        flex: 1,
        width: Dimensions.get('window').width * 0.95,
    },
    itemName:{
        fontWeight: '600',
        fontSize: 22,
    },
    itemPrice:{
        fontWeight: '600',
        fontSize: 22,
    },
    itemContainer: {
        justifyContent: 'flex-start',
        borderRadius: 16,
        padding: 10,
        height: 180,
    },
    itemChangeUp: {
        fontWeight: '600',
        fontSize: 18,
        color: Colors.up,
    },
    itemChangeDown: {
        fontWeight: '600',
        fontSize: 18,
        color: Colors.down,
    },
    itemChangePercentDown: {
        fontWeight: '600',
        fontSize: 14,
        color: Colors.down,
    },
    itemChangePercentUp: {
        fontWeight: '600',
        fontSize: 14,
        color: Colors.up,
    }
});